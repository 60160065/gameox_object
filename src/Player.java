
public class Player {
	private	char name;
	private int win=0,draw=0,lose=0;
	
	public Player (char name) {
		this.name=name;
	}
	public char getName() {
		return name;
	}
	public int getWin() {
		return win;
	}
	public int getDraw() {
		return draw;
	}
	public int getLose() {
		return lose;
	}
	public void win() {
		win++;
	}
	public void lose() {
		lose++;
	}
	public void draw() {
		draw++;
	}

	

}
