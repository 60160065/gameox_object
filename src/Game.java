import java.util.Scanner;

public class Game {
	private Board board;
	private Player x;
	private Player o;
	public Game() {
		o= new Player('O');
		x= new Player('X');
		board = new Board(x,o);
	}
	public void play() {
		showWelcome();
		showTable();
		showTurn();
		input();
		showResult();
		showBye();
		showState();
	}
	
	private void showState() {
		// TODO Auto-generated method stub
		
	}
	private void showBye() {
		// TODO Auto-generated method stub
		
	}
	private void showResult() {
		// TODO Auto-generated method stub
		
	}
	private void input() {
		Scanner kb=new Scanner(System.in);
		int n1=kb.nextInt();
		int n2=kb.nextInt();
		
	}	
	private void showTurn() {
		System.out.println(board.getCurrent().getName()+" turn.");
		
	}
	private void showWelcome() {
		System.out.println("Welcome to OX Game.");
		
	}
	private void showTable() {
		char[][]table=board.getTable();
		System.out.println("  1 2 3");
		for(int i=0;i<table.length;i++) {
			System.out.print((i+1));
			for(int j=0;j<table[i].length;j++) {
				System.out.print(" "+table[i][j]);
			}System.out.println();
		}
		
	}

	
}
